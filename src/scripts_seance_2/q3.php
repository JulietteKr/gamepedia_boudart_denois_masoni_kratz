<?php

/**
 * Importation
 */
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \gamepedia\Model as m;

/**
 * Base de données
 * Nommer son fichier de configuration de conenxion à la base de données : "dbconf.ini
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

// Question 3
$res = m\Company::CompanyLikeSony();

foreach ($res as $r) {
  echo $r->name.": \n";
  $jeux = $r->jeux;
  foreach ($jeux as $j) {
    echo $j->name."\n";
  }
  echo "\n";
}
