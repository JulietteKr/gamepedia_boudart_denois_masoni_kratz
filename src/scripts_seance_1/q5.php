<?php

/**
 * Importation
 */
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \gamepedia\Model as m;

/**
 * Base de données
 * Nommer son fichier de configuration de conenxion à la base de données : "dbconf.ini
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();


if(isset($argv[1]) && isset($argv[2])) {
  //2 arguments  !
  $numpage = $argv[1]-1; //pour que la premire page commence à 1;
  $nbParpage = $argv[2];

  $res = m\Game::jeuAPartirDe($nbParpage,$numpage*$nbParpage);
  m\Game::afficherResAvecDeck($res);
}
else {
  echo "Mauvais arguments Veuillez passer en parametre le numero de page et le nombre
  de resultats par page \n";
  echo "exemple : php scripts\q5.php 1 500";
}
