<?php

/**
 * Importation
 */
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \gamepedia\Model as m;
use \gamepedia\scripts_seance_5 as s5;

/**
 * Base de données
 * Nommer son fichier de configuration de conenxion à la base de données : "dbconf.ini
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();



// routes

$app= new \Slim\Slim;

$app->response->headers->set('Content-Type', 'application/json');

$app->notFound(function(){
  echo json_encode(array("msg"=>"url non trouvee"));
});
$app->get('/',function(){
  echo json_encode("youhou", JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
});

$app->get('/api/games/:id', function ( $id) use($app){


  //echo s5\q1::get_donnees_jeu($id);
    echo s5\q1::get_donnees_jeu_Correction($id,$app);

})->name("games");

$app->get('/api/comments/:id', function ( $id) use($app){

	s5\q1::retournerUnCommentaire($id);

})->name("comm");

$app->get('/api/games', function () use ($app) {
  $page = $app->request()->get('page');
  $page=filter_var($page,FILTER_SANITIZE_STRING);
  echo s5\q1::get_liste_jeu($page);
  //echo s5\q1:: get_donnees_game($app);
})->name('allgames');

$app->get('/api/games/:id/comments', function ($id) use ($app) {
  if(!filter_var($id,FILTER_VALIDATE_INT)) echo json_encode(array(""=>"ID incorrect"),JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  else echo s5\q1::get_commentaires_jeu($id);
})->name('commentaires');

/*
* pour le post : envoyer un json du type : 
*
* {
    "titre": "TITRE",
    "contenu": "TEST",
    "idUser": 7265
}
   
*
**/ 
$app->post('/api/games/:id/comments', function ($id) use ($app) {
	
	$json = $app->request->getBody();
	$data = json_decode($json, true);
	s5\q1::enregistrerCommentaire($data,$id);
});

$app->get('/api/games/:id/characters', function ($id) use ($app) {
    if(!filter_var($id,FILTER_VALIDATE_INT)) echo json_encode(array(""=>"ID incorrect"));
    else echo s5\q1::get_commentaires_characters($id);
})->name('characters');

$app->run();
