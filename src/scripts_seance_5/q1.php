<?php


/**
 * Importation
 */
namespace gamepedia\scripts_seance_5;

require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use \gamepedia\Model as m;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Base de données
 * Nommer son fichier de configuration de conenxion à la base de données : "dbconf.ini
 */
$db = new DB();
$db->addConnection(parse_ini_file('dbconf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

class q1 {
  /**
  * id jeu
  * doit retourner les donnees du jeu en JSON
  *
  */
  public static function get_donnees_jeu($id)
  {
    $jeu = m\Game::getGameById($id);
    if($jeu!=null){
      $name=$jeu->name;
      $alias=$jeu->alias;
      $deck=$jeu->deck;
      $description=$jeu->description;
      $original_release_date=$jeu->original_release_date;
      $array=[
        "id"=>$id,
        "name"=>$name,
        "alias"=>$alias,
        "deck"=>$deck,
        "description"=>$description,
        "original_release_date"=>$original_release_date,
      ];
    } else {
      $array=[
        "msg"=>"Game $id not found",
      ];
    }
    $json=json_encode($array,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    return $json;
  }

  /**
  *  jeu
  * doit retourner les donnees du jeu en JSON
  *
  */
  public static function get_donnees_jeu_2($jeu)
  {
    $app=\Slim\Slim::getInstance();
    if($jeu!=null){
      $name=$jeu->name;
      $alias=$jeu->alias;
      $deck=$jeu->deck;
      $description=$jeu->description;
      $original_release_date=$jeu->original_release_date;
      $url=$app->urlFor("games",array("id"=>$jeu->id));
	  $comments=$app->urlFor("commentaires",array("id"=>$jeu->id));
	  $characters=$app->urlFor("characters",array("id"=>$jeu->id));
      $game=[
        "id"=>$jeu->id,
        "name"=>$name,
        "alias"=>$alias,
        "deck"=>$deck,
        "description"=>$description,
        "original_release_date"=>$original_release_date,
      ];
    } else {
      $game=[
        "msg"=>"Game $id not found",
      ];
      $url="";
    }
    $array=[
      "game" => $game,
      "link"=> [
        "self"=>[
          "href"=>$url,
          ],
		 "comments"=>[
			"href"=>$comments,
          ],
		  "characters"=>[
			"href"=>$characters,
          ],
      ],
    ];
    return $array;
  }

/**
* Retourne les 200 premiers jeux
*/
  public static function get_liste_jeu($page){
    $app=\Slim\Slim::getInstance();
    $jeux=m\Game::listerLes200PremiersJeux($page*200);
    $games=array();
    foreach ($jeux as $j) {
      array_push($games, q1::get_donnees_jeu_2($j));
    }
    $url=$app->urlFor("allgames")."?page=";

    if($page==0){
      $prev=$page;
    }else {
      $prev=$page-1;
    }
    $next=$page+1;

    $array=array("games"=>$games,
                "links"=>[
                  "prev"=>[
                    "href"=>$url.$prev,
                    ],
                  "next"=>[
                    "href"=>$url.$next,
                    ],

                  ]);
    $json=json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    return $json;
  }

    public static function get_donnees_game($app){
        $array = ["gm"=>"Game"];
        $t=[];
      for ($id=1;$id <201;$id++){

          $res=json_decode(self::get_donnees_jeu_Correction($id,$app));
          $t[]=$res;
      }
      $array['id']=$t;

      return json_encode($array);
    }

    public static function get_donnees_jeu_Correction($id,$app)
    {
        try{
            $g=m\Game::select('id','name','alias','deck','description','original_release_date','created_at')
                ->where("id","=",$id)->firstOrFail();
        }catch (ModelNotFoundException $e){
            $app->response->setStatus(404);
            echo json_encode(["msg"=>"game $id not found"]);
            return null;
        }
        return json_encode($g,JSON_FORCE_OBJECT | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
    * Retourne l'ensemble des commentaires d'un jeu
    */
    public static function get_commentaires_jeu($id)
    {

      $app=\Slim\Slim::getInstance();
      $commentaires=m\Commentaire::commentairesJeu($id);
      $array=array();
      foreach ($commentaires as $c) {
          $user=m\Utilisateur::find($c->id_utilisateur);



          $com=[
            "id"=>$c->id,
            "titre"=>$c->titre,
            "contenu"=>$c->contenu,
            "dateCreation"=>$c->dateCreation,
            "user"=>[
                "name"=>$user->nom,
                "prenom"=>$user->prenom,
                "id"=>$user->id
            ]
          ];
          array_push($array,$com);
    }
    return json_encode($array,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  }

  public static function get_commentaires_characters($id){
      $game = m\Game::find($id);
      $characters= $game->charactersJeu()->get();
      $array=array();
      foreach ($characters as $c){
          $com=[
              "id"=>$c->id,
              "name"=>$c->name,
              "links"=>[
                  "self"=> [ "href"=> "/api/characters/".$c->id],

              ]
          ];
          array_push($array,$com);

      }
      return json_encode($array,JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

  }
  /*
  * enregistre un commentaire dans la base de données (post)
  */
  public static function enregistrerCommentaire($data,$id){
    $app=\Slim\Slim::getInstance();
	if(!isset($data['titre']) || !isset($data['contenu']) || !isset($data['idUser']) ) {
		$app->response->setStatus(400);
		echo json_encode(array("error" => "Valeur manquante"));
	} 
	else{
		$titre = filter_var($data['titre'],FILTER_SANITIZE_STRING);
		$contenu = filter_var($data['contenu'],FILTER_SANITIZE_STRING);
		$idUser= $data['idUser'];
		if(!filter_var($idUser,FILTER_VALIDATE_INT)){
			$app->response->setStatus(500);
			echo json_encode(array("error" => "Format de l'id invalide"),JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		}
		else{
			try{
				$c = new m\Commentaire();
				$c->titre=$titre;
				$c->contenu=$contenu;
				$c->dateCreation=date("Y-m-d");
				$c->id_utilisateur=$idUser;
				$c->id_game=$id;
				$c->save();
				$idCommentaire = $c->id;
				$app->response->headers->set("Location",$app->urlFor("comm",array("id"=>$idCommentaire)));
				
				
			}
			catch(mysqli_sql_exception $e){
				$app->response->setStatus(500);
				echo json_encode(array("error" => "Exception MySQL : ".$e.getMessage()),JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
			}
		}
	}
	
  }
  
  public static function retournerUnCommentaire($id){
	  $c = m\Commentaire::find($id);
	  $titre = $c->titre;
	  $contenu= $c->contenu;
	  $idUser=$c->id_utilisateur;
	  echo json_encode(array(
				"data"=>array("titre"=>$titre,"contenu"=>$contenu,"idUser"=>$idUser)),JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
  }
}
