<?php

//Question 2
class Annonce extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'annonce';
  protected $primaryKey = 'id';
  public $timestamps = 'false';


  /**
  * Retourne la liste des compagnies installées dans un pays donné
  */
  public function categories($pays){
    return $this->hasMany('Categorie','id');
  }

  public function photos($pays){
    return $this->hasMany('Photo','id');
  }
}

class Photo extends \Illuminate\Database\Eloquent\Model{
  protected $table = 'photo';
  protected $primaryKey = 'id';
  public $timestamps = 'false';


  /**
  * Retourne la liste des compagnies installées dans un pays donné
  */
  public function annonce($pays){
    return $this->belongsTo('Annonce','id');
  }
}


//Question 3.1
  $obj = Annonce::find(22)->photos()->get();
//Question 3.2
$obj = Annonce::find(22)->photos()
->where("taille_octet",">",100000)
->get();
//Question 3.3
Photo::groupBy("idAnnonce")
->having("count",'>',3)
->annonce()
->get();
// ou : $ex = Annonce::has("photos",">", 3)->get();


//Question 3.4
Photo::groupBy("idAnnonce")
->having("taille_octet",'>',100000)
->annonce()
->get();
/*ou : $ex = Annonce::whereHas("photos",function ($q){
  $q->where("taille_octet",">",100000);
})->get();*/

//Question 4
$p= new Photo();
$a22a=Annonce::find(22);
$a22a->photos()->save($p);

//Question 5
$a22b=Annonce::find(22);

$a22b->categories()->attach({42,73});
// + ->save() ?
