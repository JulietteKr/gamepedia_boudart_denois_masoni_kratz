<?php
//PARTIE 1 : 
//1.


$debut = microtime(true);

//instructions 

$fin = microtime(true);

$duree=$fin-$debut;

//2.
/*
les index MySQL servent à trier les éléments dans une table (sous forme d'un arbre binaire) 
Cela permet (entre autres) d'avoir des requetes rapides
*/



//PARTIE 2 : 

//1. 

DB::connection()->enableQueryLog();
//requetes 
DB::getQueryLog();

/*
C'est un tableau de taille n (n etant le nombbre de requetes)
	Dans chaque valeur de ce tableau, on retrouve un tableau de taille 3, composé de la requete en elle meme
	(avec des ?), les "bindings" qui sont les valeurs à placer à la place des ?, 
	et le temps d'execution
*/	

array(1) {
  [0]=>
  array(3) {
    ["query"]=>
    string(55) "select * from `company` where `location_country` like ?"
    ["bindings"]=>
    array(1) {
      [0]=>
      string(7) "%Japan%"
    }
    ["time"]=>
    float(20.02)
  }
}